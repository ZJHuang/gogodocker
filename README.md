# gobreakfast

Go server with an Nginx proxy and a MySQL database

Project structure:
```
.
├── backend
│   ├── Dockerfile
│   ├── go.mod
│   └── main.go
├── db
├── proxy
│   ├── Dockerfile
│   └── conf
├── docker-compose.yaml
└── README.md
```

## Getting started
```
$ docker-compose up -d
```

## Stop and remove the containers
```
$ docker-compose down
```
