package main

import (
    "fmt"
    "log"
    "time"
    "net/http"
    "gorm.io/gorm"
    "gorm.io/driver/mysql"
)

const (
	UserName     string = "root"
	Password     string = "root"
	Addr         string = "docker.for.mac.localhost"
	Port         int    = 3306
	Database     string = "go_breakfast"
	MaxLifetime  int    = 10
	MaxOpenConns int    = 10
	MaxIdleConns int    = 10
)

type Order struct {
	ID        int64     `gorm:"type:bigint(20) NOT NULL auto_increment;primary_key;" json:"id,omitempty"`
	Order_Name  string    `gorm:"type:varchar(20) NOT NULL;" json:"username,omitempty"`
	CreatedAt time.Time `gorm:"type:timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP" json:"created_at,omitempty"`
	UpdatedAt time.Time `gorm:"type:timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP" json:"updated_at,omitempty"`
}

func index(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Hi there, welcome to %s!", r.URL.Path[1:])
}

func main() {
  //組合sql連線字串
	addr := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True", UserName, Password, Addr, Port, Database)

  //連接MySQL
	conn, err := gorm.Open(mysql.Open(addr), &gorm.Config{})
	if err != nil {
		fmt.Println("connection to mysql failed:", err)
		return
	}

  //設定ConnMaxLifetime/MaxIdleConns/MaxOpenConns
	db, err1 := conn.DB()
	if err1 != nil {
		fmt.Println("get db failed:", err)
		return
	}

  db.SetConnMaxLifetime(time.Duration(MaxLifetime) * time.Second)
	db.SetMaxIdleConns(MaxIdleConns)
	db.SetMaxOpenConns(MaxOpenConns)

  //產生table
	conn.Debug().AutoMigrate(&Order{})
  //判斷有沒有table存在
    migrator := conn.Migrator()
    has := migrator.HasTable(&Order{})
  //has := migrator.HasTable("GG")
    if !has {
      fmt.Println("table not exist")
    }

  //http port:8000
    http.HandleFunc("/", index)
    log.Fatal(http.ListenAndServe(":8000", nil))
}
